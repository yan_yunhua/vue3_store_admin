# 基于Vue3的商城后台管理系统

## 介绍

### 1. 项目描述：

> 这是一个商城的后台管理系统，整体系统包含数据可视化图表展示、商品管理、订单管理、权限管理、商品规格管理等模块。整个技术使用 Vue3 全新的script setup语法，Vuex、Vue-router 等。
**注意：这是一个前后端分离的项目，后端引用的是别人封装好的API，所以这个项目不涉及后端开发。**


### 2. 技术架构：

**Vue3 + Vuex + Vite + Element-plus + JavaScript + WindiCSS + VueUse**

> 注意： 在开发中对Cookie的处理运用的VueUse中的useCookies，即使用vue composition-api访问和修改cookie。

### 3. 项目插件安装

```
npm install
```

其他依赖根据需要安装


1. 项目运行
```
npm run dev
```

### 4. 部分页面展示

登录: （用户名密码均为admin）
![输入图片说明](public/image/%E7%99%BB%E5%BD%95.png)

主页
![输入图片说明](public/image/%E4%B8%BB%E9%A1%B5.png)

商品管理
![商品](./public/image/%E5%95%86%E5%93%81%E7%AE%A1%E7%90%86%E9%A1%B5%E9%9D%A2.png)

商品详情
![商品详情](./public/image/%E5%95%86%E5%93%81%E8%AF%A6%E6%83%85.png)

修改商品信息
![商品信息](./public/image/%E4%BF%AE%E6%94%B9%E5%95%86%E5%93%81%E4%BF%A1%E6%81%AF.png)

修改商品规格
![修改](./public/image/%E5%BC%B9%E5%87%BA%E6%A1%86.png)

菜单权限管理
![权限管理](./public/image/%E6%9D%83%E9%99%90%E7%AE%A1%E7%90%86%E9%A1%B5%E9%9D%A2.png)

管理员
![admin](./public/image/%E7%AE%A1%E7%90%86%E5%91%98.png)

图库
![图库管理](./public/image/%E5%9B%BE%E5%BA%93.png)