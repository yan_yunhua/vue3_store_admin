/*
 * @Description: 拦截器
 * @Date: 2023-03-28 15:14:51
 * @LastEditTime: 2023-04-17 19:44:14
 * @Author: Black
 * @FilePath: \store_admin\src\axios.js
 */
import axios from 'axios'
import { getToken } from '~/composables/auth';
import { toast } from '~/composables/util'
import store from '~/store'

// 创建实例
const service = axios.create({
	// baseURL: "/api",
	baseURL: import.meta.env.VITE_APP_BASE_API
})

// 添加请求拦截器
service.interceptors.request.use((config) => {
	// 在发送请求之前做些什么
	// 向 header 头自动添加 token，存储token和用户相关信息
	// console.log(config);
	const token = getToken()

	if (token) {
		config.headers["token"] = token
	}
	return config;
}, (error) => {
	// 对请求错误做些什么
	return Promise.reject(error);
})

// 添加响应拦截器: 错误提示统一交给响应 拦截器处理
service.interceptors.response.use((response) => {
	// 对响应数据做点什么
	return response.data.data;
}, (error) => {
	const msg = error.response.data.msg || "请求失败"
	if (msg == "非法token, 请先登录!") {
		store.dispatch("logout").finally(() => location.reload())
	}
	// 对响应错误做点什么
	toast(msg, "error")
	return Promise.reject(error);
})

export default service