/*
 * @Description: 规格管理页面的api接口引入
 * @Date: 2023-04-06 18:29:03
 * @LastEditTime: 2023-04-06 18:30:56
 * @Author: Black
 * @FilePath: \store_admin\src\api\skus.js
 */
import axios from '~/axios'

export function getSkusList(page) {
  return axios.get(`/admin/skus/${page}`)
}

export function createSkus(data) {
  return axios.post("/admin/skus", data)
}

export function updateSkus(id, data) {
  return axios.post("/admin/skus/" + id, data)
}

export function deleteSkus(ids) {
  ids = !Array.isArray(ids) ? [ids] : ids
  return axios.post(`/admin/skus/delete_all`, { ids })
}

export function updateSkusStatus(id, status) {
  return axios.post(`/admin/skus/${id}/update_status`, {
    status
  })
}

