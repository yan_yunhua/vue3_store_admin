/*
 * @Description: 管理员菜单权限管理接口
 * @Date: 2023-04-05 18:55:12
 * @LastEditTime: 2023-04-05 23:01:53
 * @Author: Black
 * @FilePath: \store_admin\src\api\rule.js
 */
import axios from '~/axios'

// 获取菜单权限列表
export function getRuleList(page) {
  return axios.get(`/admin/rule/${page}`)
}

// 创建菜单权限
export function createRule(data) {
  return axios.post("/admin/rule", data)
}

// 更新菜单权限
export function updateRule(id, data) {
  return axios.post("/admin/rule/" + id, data)
}

// 更新菜单权限状态
export function updateRuleStatus(id, status) {
  return axios.post(`/admin/rule/${id}/update_status`, {
    status
  })
}

// 删除菜单权限
export function deleteRule(id) {
  return axios.post(`/admin/rule/${id}/delete`)
}