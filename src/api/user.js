/*
 * @Description: 
 * @Date: 2023-04-11 14:35:24
 * @LastEditTime: 2023-04-11 15:06:41
 * @Author: Black
 * @FilePath: \store_admin\src\api\user.js
 */
import axios from '~/axios'
import { queryParams } from "~/composables/util"

// 获取用户列表
export function getUserList(page, query = {}) {
  let r = queryParams(query)
  return axios.get(`/admin/user/${page}${r}`)
}

// 修改用户状态
export function updateUserStatus(id, status) {
  return axios.post(`/admin/user/${id}/update_status`, { status })
}

// 创建用户
export function createUser(data) {
  return axios.post(`/admin/user`, data)
}

// 更新用户信息
export function updateUser(id, data) {
  return axios.post(`/admin/user/${id}`, data)
}

// 删除用户
export function deleteUser(id) {
  return axios.post(`/admin/user/${id}/delete`)
}