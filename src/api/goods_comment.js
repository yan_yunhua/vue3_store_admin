/*
 * @Description: 商品评论数据api接口
 * @Date: 2023-04-11 17:49:38
 * @LastEditTime: 2023-04-11 17:52:21
 * @Author: Black
 * @FilePath: \store_admin\src\api\goods_comment.js
 */
import axios from '~/axios'
import { queryParams } from "~/composables/util"

export function getGoodsCommentList(page,query = {}){
    let r = queryParams(query)
    return axios.get(`/admin/goods_comment/${page}${r}`)
}


export function updateGoodsCommentStatus(id,status){
    return axios.post(`/admin/goods_comment/${id}/update_status`,{
        status
    })
}

export function reviewGoodsComment(id,data){
    return axios.post(`/admin/goods_comment/review/${id}`,{
        data
    })
}

