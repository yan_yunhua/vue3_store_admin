/*
 * @Description: 
 * @Date: 2023-04-11 22:43:46
 * @LastEditTime: 2023-04-13 22:45:15
 * @Author: Black
 * @FilePath: \store_admin\src\api\sysconfig.js
 */
import axios from "~/axios"

export function getSysconfig() {
  return axios.get(`/admin/sysconfig`)
}

export function setSysconfig(data) {
  return axios.post(`/admin/sysconfig`, data)
}

export const uploadAction = import.meta.env.VITE_APP_BASE_API + "/admin/sysconfig/upload"