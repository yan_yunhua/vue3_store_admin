/*
 * @Description: 角色管理页面的api接口引入
 * @Date: 2023-04-05 22:48:24
 * @LastEditTime: 2023-04-06 18:29:26
 * @Author: Black
 * @FilePath: \store_admin\src\api\role.js
 */
import axios from "~/axios";

// 获取角色列表
export function getRoleList(page) {
  return axios.get("/admin/role/" + page)
}

// 创建角色
export function createRole(data) {
  return axios.post("/admin/role", data)
}

// 更新角色
export function updateRole(id, data) {
  return axios.post("/admin/role/" + id, data)
}

// 更新角色权限状态
export function updateRoleStatus(id, status) {
  return axios.post(`/admin/role/${id}/update_status`, {
    status
  })
}

// 删除菜单权限
export function deleteRole(id) {
  return axios.post(`/admin/role/${id}/delete`)
}

// 配置角色权限
export function setRoleRules(id, rule_ids) {
  return axios.post("/admin/role/set_rules", {
    id, rule_ids
  })
}