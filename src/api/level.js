/*
 * @Description: 会员等级数据api接口
 * @Date: 2023-04-10 23:19:15
 * @LastEditTime: 2023-04-10 23:25:48
 * @Author: Black
 * @FilePath: \store_admin\src\api\level.js
 */
import axios from "~/axios";

// 获取会员等级列表
export function getUserLevelList(page) {
  return axios.get("/admin/user_level/" + page)
}

// 增加会员等级
export function createUserLevel(data) {
  return axios.post("/admin/user_level", data)
}

// 更新会员等级
export function updateUserLevel(id, data) {
  return axios.post("/admin/user_level/" + id, data)
}

// 更新会员等级状态
export function updateUserLevelStatus(id, status) {
  return axios.post(`/admin/user_level/${id}/update_status`, {
    status
  })
}

// 删除会员等级
export function deleteUserLevel(id) {
  return axios.post(`/admin/user_level/${id}/delete`)
}