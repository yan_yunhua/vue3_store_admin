import axios from '~/axios'
import { queryParams } from "~/composables/util"

// 登录页面接口
export function login(username, password) {
  return axios.post("/admin/login", {
    username,
    password
  })
}

// 获取管理员信息和权限的菜单的接口
export function getinfo() {
  return axios.post("/admin/getinfo")
}

// 退出登录接口
export function logout() {
  return axios.post("/admin/logout")
}

// 修改密码
export function updatepassword(data) {
  return axios.post("/admin/updatepassword", data)
}

// 获取管理员列表
export function getManagerList(page, query = { limit: 10 }) {
  let r = queryParams(query)
  return axios.get(`/admin/manager/${page}${r}`)
}

// 修改管理员状态
export function updateManagerStatus(id, status) {
  return axios.post(`/admin/manager/${id}/update_status`, { status })
}

// 创建管理员
export function createManager(data) {
  return axios.post(`/admin/manager`, data)
}

// 更新管理员信息
export function updateManager(id, data) {
  return axios.post(`/admin/manager/${id}`, data)
}

// 删除管理员
export function deleteManager(id) {
  return axios.post(`/admin/manager/${id}/delete`)
}