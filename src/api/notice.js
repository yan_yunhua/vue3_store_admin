/*
 * @Description: 
 * @Date: 2023-04-03 23:03:51
 * @LastEditTime: 2023-04-04 13:41:35
 * @Author: Black
 * @FilePath: \store_admin\src\api\notice.js
 */
import axios from '~/axios';

export function getNoticeList(page) {
  return axios.get(`/admin/notice/${page}`)
}

export function createNotice(data) {
  return axios.post("/admin/notice", data)
}

export function updateNotice(id, data) {
  return axios.post(`/admin/notice/${id}`, data)
}

export function deleteNotice(id) {
  return axios.post(`admin/notice/${id}/delete`)
}