import axios from "~/axios"

// 后台统计 CountTo 卡片部分统计数据
export function getStatistics1() {
  return axios.get("/admin/statistics1")
}

// 后台统计 Card 统计店铺/商品提示和交易信息提示部分数据
export function getStatistics2() {
  return axios.get("/admin/statistics2")
}

// 后台统计 ECharts 图表数据接口
export function getStatistics3(type) {
  return axios.get("/admin/statistics3?type=" + type)
}