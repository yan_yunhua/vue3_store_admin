/*
 * @Description: 优惠券数据api接口
 * @Date: 2023-04-06 22:43:26
 * @LastEditTime: 2023-04-06 22:46:43
 * @Author: Black
 * @FilePath: \store_admin\src\api\coupon.js
 */
import axios from '~/axios';

export function getCouponList(page) {
  return axios.get(`/admin/coupon/${page}`)
}

export function createCoupon(data) {
  return axios.post("/admin/coupon", data)
}

export function updateCoupon(id, data) {
  return axios.post(`/admin/coupon/${id}`, data)
}

export function deleteCoupon(id) {
  return axios.post(`admin/coupon/${id}/delete`)
}

export function updateCouponStatus(id) {
  return axios.post(`/admin/coupon/${id}/update_status`, {
    status: 0
  })
}