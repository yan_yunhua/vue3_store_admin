/*
 * @Date: 2023-04-02 15:27:30
 * @Author: Black
 * @LastEditTime: 2023-04-03 13:22:39
 * @FilePath: \store_admin\src\api\image_class.js
 * @Description: 封装图库模块的接口
 * @function: 
 */
import axios from "~/axios";

/**
 * @function 获取图片分类标签
 * @param {Number} page 
 * @returns api
 */
export function getImageClassList(page) {
  return axios.get("/admin/image_class/" + page)
}

export function createImageClass(data) {
  return axios.post("/admin/image_class", data)
}

export function updateImageClass(id, data) {
  return axios.post("/admin/image_class/" + id, data)
}

export function deleteImageClass(id) {
  return axios.post(`admin/image_class/${id}/delete`)
}
