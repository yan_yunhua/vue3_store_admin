/*
 * @Date: 2023-04-03 13:23:00
 * @Author: Black
 * @LastEditTime: 2023-04-03 20:43:10
 * @FilePath: \store_admin\src\api\image.js
 * @Description: 图库管理页面获取图片的 api 接口方法
 */
import axios from "~/axios";

/**
 * @author: Black
 * @description: 根据 id 和 page 参数获取图片列表
 * @param {Number} id 图库ID
 * @param {Number} page 分页页码 默认获取第一页的数据
 * @return {*}
 */
export function getImageList(id, page = 1) {
  return axios.get(`/admin/image_class/${id}/image/${page}`)
}

/**
 * @description: 更新图片的接口
 * @param {Number} id 图片ID
 * @param {String} name 图片的名称
 * @return {*} axios.post 方法
 * @author: Black
 */
export function updateImage(id, name) {
  return axios.post(`/admin/image/${id}`, { name })
}

/**
 * @description: 删除图片的接口
 * @param {Array} ids 图片ID组成的一维数组
 * @return {*} axios.post 方法
 * @author: Black
 */
export function deleteImage(ids) {
  return axios.post("/admin/image/delete_all", { ids })
}

export const uploadImageAction = import.meta.env.VITE_APP_BASE_API + "/admin/image/upload"