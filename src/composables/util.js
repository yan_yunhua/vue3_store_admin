/*
 * @Date: 2023-03-28 18:24:39
 * @Author: Black
 * @LastEditTime: 2023-04-09 19:56:01
 * @FilePath: \store_admin\src\composables\util.js
 * @Description: 封装部分提示功能
 *  1. 登录验证后的信息提示 (Success/Error/Warning) toast()
 *  2. Alert弹出提示框 showAlert()
 *  3. 全屏的loading加载功能
 *  4. 将query对象转成url参数
 *  5. 上移、下移
 *  6. sku排列算法
 */

import { ElNotification, ElMessageBox } from 'element-plus'
import nprogress from 'nprogress'

/**
 * @description: 封装登录成功和失败的信息提示
 * @author: Black
 * @param {String} message 信息提示的内容
 * @param {'success'|'error'|'warning'} [type] 提示类型默认成功 success
 * @param {Boolean} [dangerouslyUseHTMLString]  是否将 message 属性作为 HTML 片段处理
 * @return {*} 
 */
export function toast(message, type = "success", dangerouslyUseHTMLString = true) {
  ElNotification({
    message,
    type, // type类型: success, error, warning, info
    dangerouslyUseHTMLString,
    duration: 3000,
  })
}


// 显示全屏loading
export function showFullLoading() {
  nprogress.start()
}

// 隐藏全屏loading
export function hideFullLoading() {
  nprogress.done()
}

/**
 * @description: Alert弹出提示框
 * @param {String} content 提示内容
 * @param {String} title 提示标题 默认为"Warning"
 * @param {'warning'|'success'|'error'} type 提示框类型
 * @return {*} ElMessageBox.confirm
 * @author: Black
 */
export function showAlert(content = "提示内容", title = "Warning", type = "warning") {
  return ElMessageBox.confirm(
    content,
    title,
    {
      confirmButtonText: '确认',
      cancelButtonText: '取消',
      type,
    }
  )
}

/**
 * @description: 弹出输入框
 * @param {String} tip 提示标题
 * @param {String} value 从原对象获取来的数据存放在输入框内
 * @return {*} ElMessageBox.confirm
 * @author: Black
 */
export function showPrompt(tip, value = '') {
  return ElMessageBox.prompt(tip, {
    cancelButtonText: '取消',
    confirmButtonText: '确认',
    inputValue: value
  })
}

// 将query对象转成url参数
export function queryParams(query) {
  let q = []
  for (const key in query) {
    if (query[key]) {
      q.push(`${key}=${encodeURIComponent(query[key])}`)
    }
  }
  let r = q.join("&")
  r = r ? ("?" + r) : ""
  return r
}

// 上移
export function useArrayMoveUp(arr, index) {
  swapArray(arr, index, index - 1)
}

// 下移
export function useArrayMoveDown(arr, index) {
  swapArray(arr, index, index + 1)
}

// 调换数组的索引
/**
 * @description: 该函数主要通过数组原型的splice方法实现根据索引调换数组值的位置
 * splice方法：通过删除或替换现有元素或者原地添加新的元素来修改数组, 并以数组形式返回被修改的内容, 此方法会改变原数组。
 * @param {Array} arr
 * @param {Number} index1
 * @param {Number} index2
 * @return {Array} 返回被修改后的数组（调换索引后的数组）
 * @author: Black
 */
function swapArray(arr, index1, index2) {
  arr[index1] = arr.splice(index2, 1, arr[index1])[0]
  return arr
}

// sku排列算法
export function cartesianProductOf() {
  return Array.prototype.reduce.call(arguments, function (a, b) {
    var ret = [];
    a.forEach(function (a) {
      b.forEach(function (b) {
        ret.push(a.concat([b]));
      });
    });
    return ret;
  }, [
    []
  ])
}
