import { useCookies } from "@vueuse/integrations/useCookies"

// 封装 token 的方法
const tokenKey = "admin-token"
const cookie = useCookies()

// 设置token
export function setToken(token) {
  return cookie.set(tokenKey, token)
}

// 获取token
export function getToken() {
  return cookie.get(tokenKey)
}

// 删除token
export function removeToken() {
	return cookie.remove(tokenKey)
}