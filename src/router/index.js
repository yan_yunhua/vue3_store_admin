// 引入 Vue Router 模块的 createRouter 与 CreateWebHashHistory 方法
import { createRouter, createWebHashHistory } from 'vue-router'
import Index from '~/views/index.vue'
import NotFound from '~/views/error.vue'
import Login from '~/views/login.vue'
import Admin from '~/layouts/admin.vue'
import GoodsList from '~/views/goods/list.vue'
import CategoryList from '~/views/category/list.vue'
import UserList from '~/views/user/list.vue'
import OrderList from '~/views/order/list.vue'
import CommentList from '~/views/comment/list.vue'
import ImageList from '~/views/image/list.vue'
import NoticeList from '~/views/notice/list.vue'
import SettingBase from '~/views/setting/base.vue'
import CouponList from '~/views/coupon/list.vue'
import ManagerList from '~/views/manager/list.vue'
import AccessList from '~/views/access/list.vue'
import RoleList from '~/views/role/list.vue'
import SkusList from '~/views/skus/list.vue'
import LevelList from '~/views/level/list.vue'
import SettingBuy from '~/views/setting/buy.vue'
import SettingShip from '~/views/setting/ship.vue'
import DistributionIndex from '~/views/distribution/index.vue'
import DistributionSetting from '~/views/distribution/setting.vue'

// 1.1 定义路由

// 默认路由，所有用户共享
const routes = [
	{
		path: '/',
		name: "admin",
		component: Admin,
	},
	{
		path: '/:pathMatch(.*)*',
		name: 'NotFound',
		component: NotFound
	},
	{
		path: '/login',
		component: Login,
		meta: {
			title: "登录"
		}
	}
]

// 动态路由，用于匹配菜单动态添加路由
const asyncRoutes = [{
	path: "/",
	name: "/",
	component: Index,
	meta: {
		title: "后台首页"
	}
}, {
	path: "/goods/list",
	name: "/goods/list",
	component: GoodsList,
	meta: {
		title: "商品管理"
	}
}, {
	path: "/category/list",
	name: "/category/list",
	component: CategoryList,
	meta: {
		title: "分类列表 "
	}
}, {
	path: "/user/list",
	name: "/user/list",
	component: UserList,
	meta: {
		title: "用户管理"
	}
}, {
	path: "/order/list",
	name: "/order/list",
	component: OrderList,
	meta: {
		title: "订单列表"
	}
}, {
	path: "/comment/list",
	name: "/comment/list",
	component: CommentList,
	meta: {
		title: "评价列表"
	}
}, {
	path: "/image/list",
	name: "/image/list",
	component: ImageList,
	meta: {
		title: "图库列表"
	}
}, {
	path: "/notice/list",
	name: "/notice/list",
	component: NoticeList,
	meta: {
		title: "公告列表"
	}
}, {
	path: "/setting/base",
	name: "/setting/base",
	component: SettingBase,
	meta: {
		title: "配置"
	}
}, {
	path: "/coupon/list",
	name: "/coupon/list",
	component: CouponList,
	meta: {
		title: "优惠券列表"
	}
}, {
	path: "/manager/list",
	name: "/manager/list",
	component: ManagerList,
	meta: {
		title: "管理员管理"
	}
}, {
	path: "/access/list",
	name: "/access/list",
	component: AccessList,
	meta: {
		title: "菜单权限管理"
	}
}, {
	path: "/role/list",
	name: "/role/list",
	component: RoleList,
	meta: {
		title: "角色管理"
	}
}, {
	path: "/skus/list",
	name: "/skus/list",
	component: SkusList,
	meta: {
		title: "规格管理"
	}
}, {
	path: "/level/list",
	name: "/level/list",
	component: LevelList,
	meta: {
		title: "会员等级"
	}
}, {
	path: "/setting/buy",
	name: "/setting/buy",
	component: SettingBuy,
	meta: {
		title: "支付设置"
	}
}, {
	path: "/setting/ship",
	name: "/setting/ship",
	component: SettingShip,
	meta: {
		title: "物流设置"
	}
}, {
	path: "/distribution/index",
	name: "/distribution/index",
	component: DistributionIndex,
	meta: {
		title: "分销员管理"
	}
}, {
	path: "/distribution/setting",
	name: "/distribution/setting",
	component: DistributionSetting,
	meta: {
		title: "分销设置"
	}
}]


// 1.2 创建路由对象, 创建路由实例并传递 `routes` 配置
export const router = createRouter({
	// 内部提供了 history 模式的实现
	// history: createWebHistory()
	// 为了简单起见，我们在这里使用 hash 模式
	history: createWebHashHistory(),
	// routes: routes 可省略一个
	routes
})

// 动态添加路由的方法
export function addRoutes(menus) {
	// 是否有新路由
	let hasNewRoutes = false
	const findAndAddRoutesByMenus = (arr) => {
		// forEach() 方法对数组的每个元素执行一次给定的回调函数, 无返回值
		arr.forEach(e => {
			// console.log(e)
			// find() 方法返回数组中满足提供的测试函数的第一个元素的值, 否则返回undefined
			let item = asyncRoutes.find(obj => obj.path == e.frontpath)
			// console.log(item)
			if (item && !router.hasRoute(item.path)) {
				router.addRoute("admin", item)
				hasNewRoutes = true
			}
			if (e.child && e.child.length > 0) {
				findAndAddRoutesByMenus(e.child)
			}
		})
	}
	findAndAddRoutesByMenus(menus)
	// console.log(router.getRoutes())
	return hasNewRoutes
}		