import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/display.css'
import App from './App.vue'
import { router } from './router'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import store from '~/store'

// 创建并挂载根实例
const app = createApp(App)

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

// 注册路由
app.use(router)
app.use(store)
app.use(ElementPlus)

// 引入 windCSS 文件
import 'virtual:windi.css'
import '~/permission'
import "nprogress/nprogress.css"

// 引入全局自定义指令文件
import permission from '~/directives/permission'
app.use(permission)

// 进行应用挂载
app.mount('#app')
