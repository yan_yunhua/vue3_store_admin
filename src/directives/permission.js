import store from "~/store"

// 判断当前管理员是否有某种权限
function hasPermission(value, el = false) {
  if (!Array.isArray(value)) {
    throw new Error(`缺少权限,需要配置权限: v-permission="[info(接口),POST/GET]"`)
  }
  const hasAuth = value.findIndex(v => store.state.ruleNames.includes(v)) != -1
  if (el && !hasAuth) {
    el.parentNode && el.parentNode.removeChild(el)
  }
  return hasAuth
}

export default {
  install(app) {
    // console.log(app)
    app.directive("permission", {
      mounted(el, binding) {
        // console.log(el, binding)
        // binding.value // 权限别名数组
        hasPermission(binding.value, el)
      },
    })
  }
}