import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import WindiCSS from 'vite-plugin-windicss'

import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
    // 配置文件路径别名, 方便后续引入文件的路径书写
    resolve: {
        alias: {
            // "~" 为指定的替代路径名
            "~": path.resolve(__dirname, "src")
        }
    },
    // 允许跨域处理
    server: {
        proxy: {
            // 对以/api开头的请求进行代理
            '/api': {
                // 将请求目标指定到接口服务地址
                target: 'http://ceshi13.dishait.cn',
                // 设置允许跨域
                changeOrigin: true,
                rewrite: (path) => path.replace(/^\/api/, '')
                // 设置非https请求
                // secure: false,
                // 重写路径，将/api即之前的内容清除
                // pathRewrite: {
                //   '^/api': ''
                // }
            }
        }
    },
    plugins: [vue(), WindiCSS()]

})


